



/** Function is a group of logically related statements **/ 

function functionNameA(){ 
  
  
  
}

functionNameA();

function functionNameB(argument1, argument2){// Function can have  optional arguments
  
}

functionNameB(12,44);

function functionNameC(argument1, argument2){// Function can have a optional return type
  return argument1+argument2;
}

var result =  functionNameC(12,33)


// Type of functions

function functionNameD(){// named function
  
}

function(){// Anonymous function
  
}

()=>{ // arrow function 
  
}
arg => arg*1 // inline arrow function; this is equal too  (arg)=> { return*1 }



// There some major difference between those functions, but that wasn't important right now.





/** 

How to store a peice of data or referance while a program is running

We can use either var or let or const,
Both let and const is a new methods which have some restrictions since it have a specific usecases.

var is an old mehtod which dosen't have any restrictions. we can use var for all normal purposes
*/

var name = 'Sajan'; // We can assign a string values
var age = 28; // Numerical values
var isOnline = true; // or boolean value (true/false)

var abc = function(){ // Or a referance of anonymous function.
  
};

function NamedFunction(){
  
}

var abc = NamedFunction; // Or a referance of NamedFunction function.
var abc = document; // or the referance to the document Object.
var xyz = ['Sajan','Paul']//Or an array or anything.

/** Global Objects  */

//window - which referring to  the tab of opened web page
var isMobile = window.innerWidth < 767;

// document - which is referring to the document rendered in window it can be either html5,html4,xhtml,svg or any xml based document
document.title ='Will update title in tab';


/** How to access dom elements **/

//there are many methods are available, here is the most commonly using method

//1 document.querySelectorAll(cssSelector) -> used to select All matching element using css selector 
var selectedDOMList =  document.querySelectorAll('panel[data-toggle] .btn.btnprimary');
//https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector


//2 document.querySelector(cssSelector) -> used to select first matching element using css selector 
var selectedDOM =  document.querySelector('panel[data-toggle] .btn.btnprimary');
//https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll

// How to read and update input text value

var inputElement =  document.querySelector('input#inputelement');
var textboxvalue = inputElement.value ;
inputElement.value = 'text is changed'; //changing textbox value


// How to read and update p,span,div,htag text value

var warningMsgElement =  document.querySelector('p.warning-message');
var warnText = warningMsgElement.innerText ;
warningMsgElement.value = 'text is changed'; //changing p tag content

// How to read and update inline styles of an element;
var warningMsgElement =  document.querySelector('p.warning-message');
var warningMsgColor = warningMsgElement.style.color; // '' if inline style isn't added;
warningMsgElement.style.backgroundColor ='#ff00ff';


/** Receiving user input,
 *  in javascript every user input (rather than alert(),input(),prompt()) is an event 
 * so we have to register a function as listener of the event**/

window.addEventListener('resize',(event)=>{
  conole.log(window.innerWidth,window.innerHeight,)
});
/**
 (event)=>{
  conole.log(window.innerWidth,window.innerHeight,)
}
is  a arrow function which act like a handler of the event.
 */

var button =  document.querySelector('button.btn-primary');
                                     
var buttonClickHandler = (event)=>{
  alert('Button clicked');
};
button.addEventListener('click',buttonClickHandler)

